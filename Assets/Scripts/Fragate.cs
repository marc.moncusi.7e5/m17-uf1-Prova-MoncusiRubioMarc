using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fragate : MonoBehaviour
{

    public float moveSpeed = 2f;

    private float xMin, xMax;
    private float yMin, yMax;
    private float spriteSize;
    public GameObject projectile;
    private float Cadencia=0;

    private void Start()
    {
        spriteSize = GetComponent<SpriteRenderer>().bounds.size.x * .5f;

        var cam = Camera.main;
        var camHeight = cam.orthographicSize;
        var camWidth = cam.orthographicSize * cam.aspect;

        yMin = -camHeight;
        yMax = camHeight - spriteSize;

        xMin = -camWidth + spriteSize;
        xMax = camWidth - spriteSize;
    }

    private void Update()
    {
        var direction = Vector2.down.normalized;
        direction *= moveSpeed * Time.deltaTime;

        var xValidPosition = Mathf.Clamp(transform.position.x + direction.x, xMin, xMax);
        var yValidPosition = transform.position.y + direction.y;

        transform.position = new Vector3(xValidPosition, yValidPosition, 0f);

        if (transform.position.y < yMin - spriteSize*2)
        {
            Destroy(gameObject);
        }
        Shooting();
    }

    private void Shooting()
    {
        //DISPARA PERO AL REVES XD
        Cadencia += Time.deltaTime;
        if (Cadencia > 5)
        {
            Cadencia = 0;
            Instantiate(projectile, new Vector3(transform.position.x, transform.position.y - spriteSize, 0), new Quaternion(0, 0, 0, 0));

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);
    }
}