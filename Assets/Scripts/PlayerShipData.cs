using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipData : MonoBehaviour
//{
//    private float _HorizontalInput, _VerticalInput;
//    private float _velocityX, _velocityY;
//    private float _maxSpeed = 5.0f;
//    private Rigidbody2D _rigidbody;
//    private int _acceleration = 10;



//    // Start is called before the first frame update
//    void Start()
//    {
//        _rigidbody = this.GetComponent<Rigidbody2D>();
//        _HorizontalInput = 0;
//        _VerticalInput = 0;
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        _HorizontalInput = Input.GetAxis("Horizontal");
//        _VerticalInput = Input.GetAxis("Vertical");
//        _velocityX = (Mathf.Abs(_HorizontalInput) > 0 ? 1 : 0) * Mathf.Sign(_HorizontalInput) * Time.deltaTime;
//        _velocityY = (Mathf.Abs(_VerticalInput) > 0 ? 1 : 0) * Mathf.Sign(_VerticalInput) * Time.deltaTime;
//        _rigidbody.velocity += _acceleration * new Vector2(_velocityX, _velocityY);
//        if (_rigidbody.velocity.magnitude > _maxSpeed)
//        {
//            _rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, _maxSpeed);
//        }
//    }
//}

{

    public float moveSpeed = 5f;

    private float xMin, xMax;
    private float yMin, yMax;
    private float spriteSize;
    public GameObject projectile;

    private void Start()
    {
        spriteSize = GetComponent<SpriteRenderer>().bounds.size.x * .5f;

        var cam = Camera.main;
        var camHeight = cam.orthographicSize;
        var camWidth = cam.orthographicSize * cam.aspect;

        yMin = -camHeight + spriteSize;
        yMax = camHeight - spriteSize;

        xMin = -camWidth + spriteSize;
        xMax = camWidth - spriteSize;
    }

    private void Update()
    {
        // Get buttons
        var ver = Input.GetAxis("Vertical");
        var hor = Input.GetAxis("Horizontal");


        var direction = new Vector2(hor, ver).normalized;
        direction *= moveSpeed * Time.deltaTime;

        var xValidPosition = Mathf.Clamp(transform.position.x + direction.x, xMin, xMax);
        var yValidPosition = Mathf.Clamp(transform.position.y + direction.y, yMin, yMax);

        transform.position = new Vector3(xValidPosition, yValidPosition, 0f);

        Shooting();
    }
    private void Shooting()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(projectile, new Vector3(transform.position.x, transform.position.y + spriteSize, 0), new Quaternion(0, 0, 0, 0));

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "OwnMissile")
        {
            Debug.Log("hi");
            //Restart Game UI;
            Destroy(gameObject);
        }

    }
}