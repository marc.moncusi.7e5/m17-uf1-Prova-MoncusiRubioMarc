using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{

    public float moveSpeed = 5f;

    private float xMin, xMax;
    private float yMin, yMax;

    private float _velocityY;
    private float _maxSpeed = 5.0f;
    private Rigidbody2D _rigidbody;
    private int _acceleration = 10;

    private void Start()
    {
        _rigidbody = this.GetComponent<Rigidbody2D>();

        var spriteSize = GetComponent<SpriteRenderer>().bounds.size.x * .5f;

        var cam = Camera.main;
        var camHeight = cam.orthographicSize;
        var camWidth = cam.orthographicSize * cam.aspect;

        yMin = -camHeight + spriteSize;
        yMax = camHeight - spriteSize;

        xMin = -camWidth + spriteSize;
        xMax = camWidth - spriteSize;
    }

    private void Update()
    {

        _velocityY = 1 * Time.deltaTime;
        _rigidbody.velocity += _acceleration * new Vector2(0, _velocityY);
        if (_rigidbody.velocity.magnitude > _maxSpeed)
        {
            _rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, _maxSpeed);
        }
        if (transform.position.y > yMax)
        {
            Debug.Log("hello");
            Destroy(gameObject);
        }
    }
}