using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public List<GameObject> ships;
    private Camera cam;
    private float camWidth;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
        camWidth = cam.orthographicSize * cam.aspect;

    }

    // Update is called once per frame
    void Update()
    {
        var number = Random.value;
        if(number < 0.01f)
        {
            var ship = (int) Mathf.Floor(Random.Range(0, ships.Count));
            var positionX = Random.Range(-camWidth,camWidth) ;
            Instantiate(ships[ship], new Vector3(positionX, transform.position.y , 0), new Quaternion(0, 0, 0, 0));
        }
    }
}
